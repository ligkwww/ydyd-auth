<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2021-04-25
 * Time: 16:25
 */

namespace Ydyd\Auth;
use Illuminate\Support\ServiceProvider;

class AuthUserProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {
        $this->app->singleton('auth', function ($app) {
            return new AuthManage($app);
        });
    }
}